﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Exercise
{
    private string _strNumbers;
    private int[] _numbers;

    private string _strUnknowns;
    private int[] _posOfUnknowns;

    private string _strSolutions;
    private int[] _solutions;

    // Readonly properties
    public int[] Numbers        { get { return _numbers; } }
    public int[] PosOfUnknowns  { get { return _posOfUnknowns; } }
    public int[] Solutions      { get { return _solutions; } }

    public string StrUnknowns { get { return _strUnknowns; } }

    // Manera de leer los números que puede usar el jugador
    public string StrNumbers
    {
        get { return _strNumbers; }
        set
        {
            _strNumbers = value.Trim();
            string[] _arrayNumbers = _strNumbers.Split(',');
            _numbers = new int[_arrayNumbers.Length];
            for (int i = 0; i < _arrayNumbers.Length; i++)
            {
                int numValue = 0;
                if (int.TryParse(_arrayNumbers[i].Trim(), out numValue))
                {
                    _numbers[i] = numValue;
                }
                else
                {
                    Debug.LogError("NO HA PODIDO PARSEAR " + _arrayNumbers[i]);
                }
            }
        }
    }
    /** Establecer las posiciones de las incógnitas y las soluciones
     */
    public void SetUnknownsAndSolutions(string strUnknowns, string strSolutions)
    {
        _strUnknowns = strUnknowns.Trim();
        _strSolutions = strSolutions.Trim();

        // Buscamos las posiciones de las incógnitas
        List<int> posUnkList = new List<int>();
        int lastUnknown = strUnknowns.IndexOf('_');
        while (lastUnknown >= 0)
        {
            posUnkList.Add(lastUnknown);
            lastUnknown = strUnknowns.IndexOf('_', lastUnknown + 1);
        }
        _posOfUnknowns = posUnkList.ToArray();
        _solutions = new int[_posOfUnknowns.Length];
        for (int i = 0; i < _posOfUnknowns.Length; i++)
        {
            int solution;
            if (int.TryParse(strSolutions.Substring(_posOfUnknowns[i], 1),
                out solution)) { 
                _solutions[i] = solution;
            }
            else
            {
                Debug.LogError("NO HA PODIDO PARSEAR " 
                    + strSolutions.Substring(_posOfUnknowns[i], 1));
            }
        }
    }
}