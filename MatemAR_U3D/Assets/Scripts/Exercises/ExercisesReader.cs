﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExercisesReader : MonoBehaviour
{
    const int COLS_FILE = 3;
    public int currentExercise = 0;
    public TextAsset fileExercises;
    public string[,] _inputStrings;
    public Exercise[] exercises;
    int exIndex;

    // Start is called before the first frame update
    void Awake()
    {
        exIndex = -1;
        // Array bidimensional con todo el fichero 
        // de ejercicios en crudo
        string[] rows = fileExercises.text.Split('\n');
        _inputStrings = new string[rows.Length, 3];

        // Array con la info de los ejercicios desglosada 
        exercises = new Exercise[rows.Length];

        for (int r = 0; r < rows.Length; r++)
        {
            string[] cols = rows[r].Trim().Split(';');
            for (int c = 0; c < 3; c++)
            {
                _inputStrings[r, c] = cols[c].Trim();
            }
            Exercise newExercise = new Exercise
            {
                StrNumbers = cols[0]
            };
            newExercise.SetUnknownsAndSolutions(cols[1], cols[2]);
            exercises[r] = newExercise;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public Exercise Current
    {
        get
        {
            if (exIndex < 0)  exIndex = 0;
            return exercises[exIndex];
        }
    }
    public Exercise Next
    {
        get
        {
            exIndex++;
            return exercises[exIndex];
        }
    }
    public bool EndGame
    {
        get
        {
            return exIndex >= exercises.Length - 1;
        }
    }
}
