﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageFadeOut : MonoBehaviour
{
    public float delay = 3f;
    public float duration = 6f;
    public float initialAlpha = 1f;
    public float finalAlpha = 0f;

    float initTime;
    Text text;
    Image image;
    SpriteRenderer sprite;

    // Start is called before the first frame update
    private void OnEnable()
    {
        initTime = Time.time;
        text = GetComponent<Text>();
        image = GetComponent<Image>();
        sprite = GetComponent<SpriteRenderer>();
    }
    private void Start()
    {
        ObjColor = new Color(
                ObjColor.r,
                ObjColor.g,
                ObjColor.b,
                initialAlpha);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - initTime >= delay)   // Begin & continue fade out
        {
            float newAlpha = ObjColor.a 
                +   (finalAlpha - initialAlpha) 
                    * Time.deltaTime / duration;
        }
        if (Time.time - initTime >= delay + duration)   // End
        {
            gameObject.SetActive(false);
        }
    }
    Color ObjColor  {
        get  {
            return    text ? text.color 
                    : image ? image.color
                    : sprite.color;
        }
        set
        {
            if (text) text.color = value;
            if (image) image.color = value;
            if (sprite) sprite.color = value;
        }
    }
}
