﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelHUD : MonoBehaviour
{
    public GameController gameCtrl;
    public Text textFormula;
    public Text textScore;
    string strFormula;

    private void Update()
    {
        textFormula.text = UserFormulaStr();

        textScore.text = gameCtrl.scoreCtrl.score + "\n" + gameCtrl.scoreCtrl.GetTimer();
    }
    string UserFormulaStr()
    {
        string strFormula = gameCtrl.current.StrUnknowns;

        for (int i = 0; i < gameCtrl.userNumbers.Count; i++)
        {
            int userNumber = gameCtrl.userNumbers[i];
            int posChar = gameCtrl.current.PosOfUnknowns[i];

            strFormula = strFormula.Substring(0, posChar)
                + userNumber + strFormula.Substring(posChar + 1);
        }
        return strFormula;
    }
}
