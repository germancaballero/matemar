﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchRaycast : MonoBehaviour
{
    Collider lastCollider;
    RaycastHit hit;
    Ray ray;

    // Start is called before the first frame update
    void Start()
    {
        Physics.queriesHitTriggers = true;
    }

    // Update is called once per frame
    void Update()
    {
        int layermask = int.MaxValue;

        if (Input.touchCount > 0
            || Input.GetMouseButtonUp(0))
        {
            Vector3 touchPosition;
            if (Input.touchCount > 0)
                touchPosition = Input.touches[0].position;
            else
                touchPosition = Input.mousePosition;
            ray = Camera.main.ScreenPointToRay(touchPosition);

            if (Physics.Raycast(ray, out hit,
                Mathf.Infinity, layermask, QueryTriggerInteraction.Collide))
            {
                WhenRaycastHit(hit);
            }
            else
            {
                WhenNonHitTryUnselect(hit);
            }
        }
    }
    void WhenRaycastHit(RaycastHit hit)
    {
        // When the  user hit to a number:
        if (hit.collider.GetComponent<CubeNumber>())
        {
            hit.collider.GetComponent<CubeNumber>().ReciveHit(hit);
            lastCollider = hit.collider;
        }
        else
        {
            WhenNonHitTryUnselect(hit);
        }
    }

    void WhenNonHitTryUnselect(RaycastHit hit)
    {
        if (lastCollider != null && lastCollider.GetComponent<CubeNumber>())
        {
            lastCollider.GetComponent<CubeNumber>().UnatachHit(hit);
        }
    }
}