﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardController : MonoBehaviour
{
    public AplicationManager appManager;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.B))
        {
            appManager.Begin();
        }
        if (Input.GetKey(KeyCode.R))
        {
            appManager.Restart();
        }
        if (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Q))
        {
            appManager.Quit();
        }
    }
}
