﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManipulators : MonoBehaviour
{
    public GameObject world;
    public float minScale = .2f;
    public float maxScale = 5f;
    public float rotationSpeed = 2f;

    Touch touch_0, touch_1;
    float initialDistance, 
        initialAngle,
        initialScale,
        initialRotation;
    int previouesTochCounter;
    
    // Update is called once per frame
    void Update()
    {
        /*if (Input.touchSupported)
        {*/
            if (Input.touchCount == 2)
            {
                Touch t0 = Input.touches[0];
                Touch t1 = Input.touches[1];

                float distanceBetweenToches = t0.DistanceTo(t1);
                float angleBetweenToches = t0.AngleTo(t1);

                if (previouesTochCounter != 2)
                {
                    initialDistance = distanceBetweenToches;
                    initialAngle = angleBetweenToches;
                    initialScale = world.transform.localScale.x;
                    initialRotation = world.transform.localEulerAngles.y;
                }
                float scaleFactor = distanceBetweenToches / initialDistance;
                float rotationAngle = angleBetweenToches - initialAngle;

                float scaleWithLimit = initialScale * scaleFactor;
                if (scaleWithLimit < minScale)
                    scaleWithLimit = minScale;
                if (scaleWithLimit > maxScale)
                    scaleWithLimit = maxScale;

                Vector3 finalScale3 = Vector3.one * scaleWithLimit;
                Vector3 finalRotation3 = Vector3.up * initialRotation - Vector3.up * rotationAngle * rotationSpeed;
                world.transform.localScale = finalScale3;
                world.transform.localEulerAngles = finalRotation3;
            }
            previouesTochCounter = Input.touchCount;
       /* } else
        {
            this.enabled = false;
            Debug.LogError("Touch support not pressent");
            //message.text = "Touch support not pressent";
        }*/
    }
}
