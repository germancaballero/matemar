﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class MessageText
{
    public string name;
    public GameObject guiText;
    public string text;
}

public class PanelMessages : MonoBehaviour
{
    [Header("*** Message Objects *** ")]
    public List<MessageText> messages;
    
    public void ShowMessage(string name)
    {
        foreach (var msg in messages)
        {
            if (msg.name.ToLower().Equals(name.ToLower()))
            {
                msg.guiText.SetActive(true);
                msg.guiText.GetComponent<Text>().text = msg.text;
            } 
        }
    }
}
