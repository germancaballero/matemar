﻿using UnityEngine;
using System.Collections;

public static class VectorExt
{
    public static float DistanceTo(this Touch t0, Touch t1)
    {
        return Vector2.Distance(t0.position, t1.position);
    }
    public static float AngleTo(this Touch t0, Touch t1)
    {
        return Vector2.Angle(t0.position, t1.position);
    }
    public static Transform Clear(this Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        return transform;
    }
    public static Vector2 FromXZtoXY(this Transform transform)
    {
        return new Vector2(transform.localPosition.x,
            transform.localPosition.z);
    }
    public static void SetLocalX(this Transform transform, float x)
    {
        Vector3 newPosition =
           new Vector3(x, transform.localPosition.y, transform.localPosition.z);

        transform.localPosition = newPosition;
    }
    public static void SetLocalY(this Transform transform, float y)
    {
        Vector3 newPosition =
           new Vector3(transform.localPosition.x, y, transform.localPosition.z);

        transform.localPosition = newPosition;
    }
    public static void SetLocalZ(this Transform transform, float z)
    {
        Vector3 newPosition =
           new Vector3(transform.localPosition.x, transform.localPosition.y, z);

        transform.localPosition = newPosition;
    }

}