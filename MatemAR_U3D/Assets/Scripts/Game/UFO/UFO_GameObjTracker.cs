﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UFO_GameObjTracker : UFO_MonoBehaviour
{
    public Transform transformTracker;

    public TrackerWay wayToTrack;
    public float lerpFactor = .03f;
    Quaternion initialRotation;

    void Start()
    {
        initialRotation = transform.localRotation;
    }
    // Update is called once per frame
    protected void Update()
    {
        if (wayToTrack == TrackerWay.TransformJoined)
        {
            transform.localPosition = transformTracker.localPosition;
            // Apply transformation to forward space ship direction
            transform.localRotation = transformTracker.localRotation * initialRotation;
        }
        else if (wayToTrack == TrackerWay.FollowWithLerp)
        {
            // First, move gameObj to the Ufo_Player
            transform.position = Vector3.Lerp(
                transform.position, transformTracker.position, lerpFactor);

            // Lerp Rotation when moving, to direction object direction
            transform.localEulerAngles =
                Vector3.Lerp(
                    transform.localEulerAngles,
                    new Vector3(transform.localEulerAngles.x,
                                transformTracker.localEulerAngles.y,
                                transform.localEulerAngles.z),
                    lerpFactor * 2f);
        }
    }
}
