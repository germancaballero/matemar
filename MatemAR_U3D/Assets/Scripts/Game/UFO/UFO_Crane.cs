﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnUfoCraneEvent(CraneMotion craneMotion);

public class UFO_Crane : UFO_MonoBehaviour
{
    public CraneMotion motion = CraneMotion.GoingUp;
    public float minimumLerp = 0.005f;
    public float lerpInvFactor = 200f;
    public Transform[] joins;

    public OnUfoCraneEvent OnUfoCraneEnd;

    Vector3[] posJoins;

    void Start()
    {
        posJoins = new Vector3[joins.Length];
        for (int i = 0; i < joins.Length; i++)
        {
            posJoins[i] = joins[i].localPosition;
        }
        SetState(CraneMotion.GoingUp);
    }

    // Update is called once per frame
    private void Update()
    {
        float maxDist = Vector3.Distance(posJoins[0], posJoins[posJoins.Length - 1]);
        float currentDist = Vector3.Distance(posJoins[0],
            joins[joins.Length - 1].localPosition);

        if (motion == CraneMotion.GoingDown)   // Grúa bajando
        {
            float lerpFactor = (currentDist) / lerpInvFactor + minimumLerp * 2f;
            for (int i = 0; i < posJoins.Length; i++)
            {
                joins[i].localPosition = Vector3.Lerp(
                    joins[i].localPosition, posJoins[i], lerpFactor);
            }
            if (Mathf.Abs(maxDist - currentDist) <= 0.12f)
            {
                OnUfoCraneEnd?.Invoke(motion);
                this.enabled = false;
            }
        }
        else // Grúa subiendo
        {
            float lerpFactor = (maxDist - currentDist) / lerpInvFactor + minimumLerp;

            foreach (Transform j in joins)
            {
                j.localPosition = Vector3.Lerp(
                    j.localPosition, joins[0].localPosition, lerpFactor);
            }
            if (Mathf.Abs(currentDist) < 0.12f)
            {
                OnUfoCraneEnd?.Invoke(motion);
                this.enabled = false;
            }
        }
    }
    public void SetState(CraneMotion motion)
    {
        this.motion = motion;
        this.enabled = true;
    }
}
