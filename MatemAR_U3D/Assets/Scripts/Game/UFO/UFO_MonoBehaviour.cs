﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFO_MonoBehaviour : MonoBehaviour
{
    public new GameObject gameObject;
    public new Transform transform
    {
        get
        {
            return gameObject.transform;
        }
    }
    protected UFO_StateMachine ufoSM;

    // Start is called before the first frame update
    protected void Awake()
    {
        if (!ufoSM)
        {
            ufoSM = SearchUfoSM_ThrowsParents(base.transform);
        }
    }

    UFO_StateMachine SearchUfoSM_ThrowsParents(Transform obj)
    {
        if (obj.GetComponent<UFO_StateMachine>() )
        {
            return obj.GetComponent<UFO_StateMachine>();
        }
        else if (obj != null && obj.parent!= null)
        {
            return SearchUfoSM_ThrowsParents(obj.parent);
        } else
        {
            return null;
        }
    }
}
