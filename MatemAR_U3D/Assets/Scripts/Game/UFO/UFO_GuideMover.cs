﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFO_GuideMover : UFO_MonoBehaviour
{
    public float lerpFactor = 0.01f;
    public float yVelocity = 0.1f;
    Vector2 targetDestinationPos;

    Transform top;
    Transform midle;
    Transform bottom;

    void Start()
    {
        top = ufoSM.limitPoints.Find("Top");
        midle = ufoSM.limitPoints.Find("Midle");
        bottom = ufoSM.limitPoints.Find("Bottom");
    }
    
    /// <summary>
    /// Set the position where UFO should to go, 
    /// ever go to a Cube Number or to the initial position
    /// </summary>
    /// <param name="finalPos"></param>
    public void SetTargetPosition(Vector2 finalPos)
    {
        targetDestinationPos = finalPos;

        UpdateDirectionToDestination();
    }
    public bool MovingUp()
    {
        AproachToTargetPos(lerpFactor / 9f);
        if (transform.localPosition.y < top.localPosition.y)
        {
            transform.localPosition += Vector3.up * Time.deltaTime * yVelocity;
            return false; // Don't change
        }
        else if (transform.localPosition.y > bottom.localPosition.y)
        {
            transform.localPosition = new Vector3(
                transform.localPosition.x,
                top.localPosition.y,
                transform.localPosition.z);
            return true; // Next state
        }
        else return true;// Next state
    }
    public bool MovingTo()
    {
        Vector2 newPos = AproachToTargetPos(lerpFactor);

        if (Vector2.Distance(newPos, targetDestinationPos) < 1f)
            return true; // Next state
        else
            return false; // Don't change state
    }
    public bool MovingDown()
    {
        AproachToTargetPos(lerpFactor / 3f);
        Vector3 finalPosition = new Vector3(
                transform.localPosition.x,
                midle.localPosition.y,
                transform.localPosition.z);

        if (transform.localPosition.y > midle.transform.localPosition.y)
        {
            transform.localPosition += Vector3.down * Time.deltaTime * yVelocity;
        }
        else if (transform.localPosition.y < midle.transform.localPosition.y)
        {
            transform.localPosition = finalPosition;
        }

        if (Vector3.Distance(transform.localPosition, finalPosition) < .1f)
        {
            ufoSM.crane.SetState(CraneMotion.GoingDown);
            return true; // Next state
        }
        return false;
    }
    public void Waiting()
    {
        AproachToTargetPos(lerpFactor / 3f);
        // Equal MOVING_DOWN, but nearing to cubeNumber
        if (transform.localPosition.y > bottom.localPosition.y)
            transform.localPosition += Vector3.down * Time.deltaTime * yVelocity / 3f;
        else if (transform.localPosition.y < bottom.localPosition.y)
            transform.SetLocalY(bottom.localPosition.y);
    }

    Vector2 AproachToTargetPos(float lerpFactor)
    {
        Vector2 newPos = new Vector2(transform.localPosition.x,
            transform.localPosition.z);
        newPos = Vector2.Lerp(newPos, targetDestinationPos, lerpFactor);
        transform.localPosition = new Vector3(
            newPos.x, transform.localPosition.y, newPos.y);
        return newPos;
    }
    void UpdateDirectionToDestination()
    {
        transform.LookAt(
            new Vector3(
                targetDestinationPos.x,
                transform.position.y,
                targetDestinationPos.y));
    }
}
