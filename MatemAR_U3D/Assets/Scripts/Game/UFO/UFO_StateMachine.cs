﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFO_StateMachine : MonoBehaviour
{
    public Transform limitPoints;
    public UfoState state;

    public Transform restPlatformPos;

    public UFO_GuideMover guideMover;
    public UFO_Crane crane;
    public UFO_Magnet magnet;

    // Start is called before the first frame update
    void Start()
    {
        crane.OnUfoCraneEnd += OnCraneEnd;

        GoToRestPlatformPos();
    }
    // Update is called once per frame
    void Update()
    {
        bool changeNextState = false;
        switch (state)
        {
            case UfoState.MovingUp:
                changeNextState =  guideMover.MovingUp();
                if (changeNextState)
                    state = UfoState.MovingTowards;
                break;
            case UfoState.MovingTowards:
                changeNextState = guideMover.MovingTo();
                if (changeNextState) state = UfoState.MovingDown;
                break;
            case UfoState.MovingDown:
                changeNextState = guideMover.MovingDown();
                if (changeNextState) state = UfoState.CraneGoingDown;
                break;
            case UfoState.Waiting:
                guideMover.Waiting();
                break;
        }
        /*if (changeNextState)
            Debug.Log( "----- ") */
    }    
    public void GoToRestPlatformPos()
    {
        guideMover.SetTargetPosition(restPlatformPos.FromXZtoXY());
        // Be careful, not crane
        state = UfoState.Waiting;
    }
    public void GoToCenterPos()
    {
        SetTargetPosition(new Vector2(0f, 0f));
        // Be careful, not crane
        state = UfoState.Waiting;
    }
    /// <summary>
    /// Set the position where UFO should to go, 
    /// and prepare state to move crane up
    /// </summary>
    /// <param name="finalPos"></param>
    public void SetTargetPosition(Vector2 finalPos)
    {
        crane.SetState(CraneMotion.GoingUp);
        state = UfoState.CraneGoingUp;

        guideMover.SetTargetPosition(finalPos);
    }

    void OnCraneEnd(CraneMotion motion)
    {
        Debug.Log("OnCraneEnd " + state);
        if (state == UfoState.CraneClimbingUpCube)
            GoToCenterPos();
        if (state != UfoState.Waiting &&
            state != UfoState.CraneClimbingUpCube
            && motion == CraneMotion.GoingUp)
        {
            state = UfoState.MovingUp;
        }
    }
}
