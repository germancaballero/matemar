﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFO_Magnet : UFO_MonoBehaviour
{
    public GameObject magnetObj;
    public GameObject lightAbduction;

    void Start()
    {
        lightAbduction.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        magnetObj.transform.Rotate(Vector3.up, Time.deltaTime * 200f);
        if (transform.childCount <= 2)
        {
            lightAbduction.SetActive(false);
        }
        else {
            lightAbduction.SetActive(true);

            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).GetComponent<CubeNumber>())
                {
                    transform.GetChild(i).localRotation = magnetObj.transform.localRotation;
                }
            }
        }
    }
    public void ClearCubeNumbers()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<CubeNumber>())
            {
                transform.GetChild(i).GetComponent<CubeNumber>().WillDestroy();
            }
        }
    }
}
