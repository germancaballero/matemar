﻿
public enum UfoState
{
    Waiting,
    CraneGoingUp,
    MovingUp,
    MovingTowards, 
    MovingDown,
    CraneGoingDown,
    CraneClimbingUpCube
}

public enum TrackerWay
{
    TransformJoined,
    FollowWithLerp
}
public enum CraneMotion
{
    GoingUp,
    GoingDown
}