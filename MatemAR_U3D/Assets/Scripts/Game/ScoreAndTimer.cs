﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnTimerFinish();

public class ScoreAndTimer : MonoBehaviour
{
    public int score = 0;
    public float timeForResolve = 32;
    [Header("*** Game Objects *** ")]
    public GameController gameCtrl;

    public OnTimerFinish OnTimerFinish;

    int currentFormula = 0;
    float initTime;
    float timer;

    void Awake()
    {
        currentFormula = 0;
    }
    // Start is called before the first frame update
    public void Start()
    {
        initTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        timer = timeForResolve - (Time.time - initTime);
        timer = Mathf.Round(timer * 10f) / 10f;

        if (currentFormula < gameCtrl.exercisesReader.currentExercise)
        {
            score += 100;  
        }
        else
        {
            if (timer <= 0)
            {
                gameCtrl.msgGUI.ShowMessage("Timeout");
                gameCtrl.LoadExercise(true);
                initTime = Time.time;
                return;
            }
        }
        currentFormula = gameCtrl.exercisesReader.currentExercise;
    }
    public float GetTimer()
    {
        return timer;
    }
}
