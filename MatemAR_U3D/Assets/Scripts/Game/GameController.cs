﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameController : MonoBehaviour
{
    public ExercisesReader exercisesReader;
    public ScoreAndTimer scoreCtrl;
    public CubeNumberController cubeNumCtrl;
    public PanelMessages msgGUI;
    public Exercise current;
    public List<int> userNumbers;

    // Start is called before the first frame update
    void Start()
    {
        LoadExercise(false);
    }
    private void Update()
    {
        ControlExerciseLogic();
    }

    public void LoadExercise(bool repeatExercise)
    {
        scoreCtrl.Start();
        if (!repeatExercise)
        {
            current = exercisesReader.Next;
        }
        List<CubeNumber> cubes = cubeNumCtrl.GenerateCubeNums(current.Numbers);
        foreach (CubeNumber cube in cubes)
        {
            cube.OnCollisionCube += OnCollisionNumber;
        }
        userNumbers = new List<int>();
    }
    void ControlExerciseLogic()
    {
        bool acertado = false;
        if (userNumbers.Count == current.Solutions.Length)
        {
            acertado = true;
            for (int i = 0; i < userNumbers.Count; i++)
            {
                if (userNumbers[i] != current.Solutions[i])
                    acertado = false;
            }
            Debug.Log("> ACERTADO? = " + acertado);
            if (acertado)
            {
                if (exercisesReader.EndGame)
                {
                    msgGUI.ShowMessage("End");
                }
                else
                {
                    msgGUI.ShowMessage("Correct");
                }
                LoadExercise(false);
            }
            else
            {
                msgGUI.ShowMessage("Wrong");
                LoadExercise(true);
            }
        }
    }
    void OnCollisionNumber(CubeNumber cubeNum, Transform transformCollision)
    {
        if (transformCollision.name.ToLower().Contains("magnet"))
        {
            userNumbers.Add(cubeNum.number);
        }
    }
}