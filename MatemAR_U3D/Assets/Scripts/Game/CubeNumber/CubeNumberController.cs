﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeNumberController : MonoBehaviour
{
    public float sizeRndPos = 1f;
    [Header("*** Game Objects *** ")]
    public GameObject numbersGameObj;
    public CubeNumber prefabCubeNum;
    public UFO_StateMachine ufoSM;

    Vector3 cubeNumBaseScale;
    float cubeNumInit_Y;
    float initAngleNewCubesPos;

    // Start is called before the first frame update
    void Start()
    {
        cubeNumBaseScale = numbersGameObj.transform.GetChild(0).transform.localScale;
        cubeNumInit_Y = numbersGameObj.transform.GetChild(0).transform.localPosition.y;
        initAngleNewCubesPos = Random.Range(0, 2 * Mathf.PI);
        Destroy(numbersGameObj.transform.GetChild(0).gameObject);
    }
    public List<CubeNumber> GenerateCubeNums(int[] numbers)
    {
        ufoSM.GoToRestPlatformPos();
        initAngleNewCubesPos += Mathf.PI / 7f;

        // Clean cube numbers
        numbersGameObj.transform.Clear();
        ufoSM.magnet.ClearCubeNumbers();
        // Generate new cube numbers arround de "world" center
        List<CubeNumber> cubeNumbers = new List<CubeNumber>();
        for (int i = 0; i < numbers.Length; i++)
        {
            int symbol = numbers[i];
            CubeNumber cn = SpawnCubeNumber(numbers.Length, i, symbol, initAngleNewCubesPos);
            cubeNumbers.Add(cn);
        }
        return cubeNumbers;
    }
    CubeNumber SpawnCubeNumber(int totalNumbers, int index, int symbol, float initAngle)
    {
        CubeNumber newCube = GameObject.Instantiate<CubeNumber>(prefabCubeNum);
        newCube.OnCollisionCube = OnCollisionNumber;
        newCube.SetSymbol(symbol.ToString()[0]);
        newCube.transform.parent = numbersGameObj.transform;

        Vector3 randomPos = RandomPositionArroundCenter(totalNumbers, index, initAngle);
        newCube.transform.localPosition = randomPos;
        newCube.transform.Rotate(Vector3.up,
            Random.Range(0, 360f));
        newCube.transform.localScale = cubeNumBaseScale;
        newCube.gameObject.name = "CubeNumber_" + newCube.symbol;
        return newCube;
    }
    Vector3 RandomPositionArroundCenter(int totalNumbers, int index, float initAngle)
    {
        float radians = ((float) index) / (totalNumbers + 1) * Mathf.PI * 2 + initAngle;
        float distanceToCenter = Random.Range(sizeRndPos / 2f, sizeRndPos);

        Vector3 randomPos = new Vector3(
            Mathf.Cos(radians) * distanceToCenter,
            cubeNumInit_Y,
            Mathf.Sin(radians) * distanceToCenter);
        return randomPos;
    }
    void OnCollisionNumber(CubeNumber cubeNum, Transform transformCollision)
    {
        if (transformCollision.name.ToLower().Contains("magnet"))
        {

            cubeNum.transform.parent = ufoSM.magnet.transform;
            ufoSM.crane.OnUfoCraneEnd += OnUfoCraneEnd;
            ufoSM.crane.SetState(CraneMotion.GoingUp);
            ufoSM.state = UfoState.CraneClimbingUpCube;
        }
    }
    void OnUfoCraneEnd(CraneMotion motion)
    {
        ufoSM.crane.OnUfoCraneEnd -= OnUfoCraneEnd;
        ufoSM.magnet.ClearCubeNumbers();
    }
}
