﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnCollisionCubeNumber(CubeNumber cubeNum, Transform transformCollision);

public class CubeNumber : MonoBehaviour
{
    public int number = 0;
    public char symbol = '\0';
    public ParticleSystem seletecParticles;
    public UFO_StateMachine ufoSM;
    public OnCollisionCubeNumber OnCollisionCube;
    private bool destroying;

    public static readonly char[] SYMBOLS = new char[17]{
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '.', '+', '-', 'x', '/', '=', '?' };
    
    // Start is called before the first frame update
    void Awake()
    {
        // Buscamos el número ó símbolo en el array ...
        if (symbol != '\0')
        {
            for (int i = 0; i < SYMBOLS.Length; i++)
            {
                // ... y una vez sabido ...
                if (symbol == SYMBOLS[i]) number = i;
            }
        }
        // Cambiamos el desplazamiento "U" en el UV para mostrarlo
        float offsetOld = GetComponent<Renderer>().material.mainTextureOffset.x;
        GetComponent<Renderer>().material.mainTextureOffset
            = new Vector2(((float)number) * 1f / 17f + offsetOld, 0);
    }
    
    private void Start()
    {
        if (!ufoSM)
            ufoSM = GameObject.FindObjectOfType<UFO_StateMachine>();
    }
    private void OnEnable()
    {
        seletecParticles.gameObject.SetActive(false);
        GetComponent<Renderer>().material.SetInt("_Selected", 0);
    }
    // Update is called once per frame
    void Update()
    {
        if (destroying)
        {
            transform.localScale -= (Vector3.one * 3f) * Time.deltaTime;
            transform.SetLocalY(transform.localPosition.y + 1.2f * Time.deltaTime);
            if (transform.localScale.x <= 0) { 
                Destroy(this.gameObject);
            }
        }
    }
    internal void SetSymbol(char symbol)
    {
        this.symbol = symbol;
        Awake();
    }
    public void ReciveHit(RaycastHit hit)
    {
        seletecParticles.gameObject.SetActive(true);
        GetComponent<Renderer>().material.SetInt("_Selected", 1);
        ufoSM.SetTargetPosition(this.transform.FromXZtoXY());
    }
    public void UnatachHit(RaycastHit hit)
    {
        Debug.Log("Parece que no des-selecciona");
        seletecParticles.gameObject.SetActive(false);
        GetComponent<Renderer>().material.SetInt("_Selected", 0);
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(">>> COLISION CON " + other.name);
        if (other.name.ToLower().Contains("cube"))
            transform.localPosition += Vector3.forward / 2f;
        OnCollisionCube?.Invoke(this, other.transform);
    }
    public void WillDestroy()
    {
        destroying = true;
    }
}
