﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AplicationManager : MonoBehaviour
{
    public bool activateAR;
    public string loadSceneName;
    [Header("*** Game Objects *** ")]
    public GameController gameCtrl;
    public Vuforia.VuforiaBehaviour arCamera;
    public Transform imageTarget;
    public Transform staticEnvironment;
    public Transform dynamicWorld;
    
    // Start is called before the first frame update
    void Start()
    {
        if ( arCamera )
        {
            arCamera.enabled = activateAR;
            if (activateAR)
            {
                staticEnvironment.transform.parent = imageTarget;
                dynamicWorld.transform.parent = imageTarget;
            }
        } else
        {
            if (loadSceneName != "")
            {
                SceneManager.LoadSceneAsync(loadSceneName, LoadSceneMode.Single);
            }
        }
    }
    public void Begin()
    {
        gameCtrl.gameObject.SetActive(true);
    }
    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
    public void Quit()
    {
        Application.Quit();
    }
}
