﻿Shader "German/Unlit/UnlitCubeNum3D"
{
    Properties
    {
        _MainTex		("Texture", 2D) = "white" {}
		_NumberColor	("Number Color", Color) = (1,1,1,1)
		_BackgroundColor("Background Color", Color) = (1,1,1,1)
		_SelectedColor	("Selected Color", Color) = (1,1,1,1)
		_Selected		("Selected", Int) = 0 
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _NumberColor;
			float4 _BackgroundColor;
			float4 _SelectedColor;
			int _Selected;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
				// Cogemos el color que corresponda
				col = col.a > 0 ? _NumberColor : _BackgroundColor;
				// ****** AHORA, SELECCIONAMOS COLOR Según esté seleccionado o no
				col += _SelectedColor * _Selected;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
